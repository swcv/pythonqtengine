#!/usr/bin/python3

import os
import sys
import requests

from PyQt5.QtCore import *
from PyQt5.QtGui import *

from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QTabWidget
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtWidgets import QSizePolicy

class PyQtUiPlugin():

    DIALOG_OK       = 0
    DIALOG_CANCEL   = 1

    def __init__(self, title, parentWidget):
        self.__title        = title
        self.__parentWidget = parentWidget
        self.__dialog       = PyQtUiPlugin.PyQtUiDialog(self.__parentWidget)
        self.keyStore       = {}
        print ("Plugin '%s' init" % self.__title)

    #
    # Public API:
    #
    def getTitle(self):
        return self.__title
    def OkDialog(self, title, body):
        return self.__dialog.OkDialog(title, body)
    def OkCancelDialog(self, title, body):
        return self.__dialog.OkCancelDialog(title, body)

    def addValueToLocalStore(self, key, value):
        self.keyStore[key] = value
    def getValueFromLocalStore(self, key):
        return self.keyStore.get(key, None)


    # buildLayoutTree is a helper to recursively to generate layout/widget structures
    # for the plugin, based on information which is specified by the plugin. This
    # removes a lot of code from plugins, as they store only the bluepring for the 
    # plugin visual layout:
    def buildLayoutTree(self, parent, _d):
        cmd = _d.get('type', None)
        #print(cmd)
        
        if cmd == 'layout':
            child = _d.get('widget')()
            parent.setLayout(child)

        elif cmd == 'widget':
            child = _d.get('widget')()
            parent.addWidget(child)

        elif cmd == 'label':
            child = _d.get('widget')(_d.get('name', ''))
            parent.addWidget(child)

        elif cmd == 'button':
            child = _d.get('widget')(_d.get('name', ''))
            parent.addWidget(child)

        elif cmd == 'list':
            child = _d.get('widget')()
            parent.addWidget(child)

        elif cmd == 'table':
            child = _d.get('widget')()
            child.setColumnCount(len(_d.get('headerlabels', [''])))
            child.setHorizontalHeaderLabels(_d.get('headerlabels', ['']))
            parent.addWidget(child)

        elif cmd == 'tree':
            child = _d.get('widget')()
            parent.addWidget(child)
            child.setColumnCount(len(_d.get('headerlabels', [''])))
            child.setHeaderLabels(_d.get('headerlabels', ['']))

        elif cmd == 'lineedit':
            child = _d.get('widget')()
            parent.addWidget(child)
            if False == _d.get('editable', True):
                child.setDisabled(True)

        else:
            print ("Unknown rebuild command %s" % cmd)
            return

        if None != _d.get('cbClick', None):
            child.clicked.connect(lambda: _d.get('cbClick')())
        if None != _d.get('cbItemChanged', None):
            child.itemChanged.connect(lambda: _d.get('cbItemChanged')())
        if None != _d.get('cbTextChanged', None):
            child.textChanged.connect(lambda: _d.get('cbTextChanged')())


        if None != _d.get('key', None):
            self.addValueToLocalStore(_d.get('key'), child)

        for c in _d.get('content', []):
            self.buildLayoutTree(child, c)

    #
    # PyQtUiDialog class is a template for creating blocking modal dialogs. It offers
    # tools for creating OK or OK/CANCEL dialogs, and is able to return the selection
    # state the user selects.
    #
    class PyQtUiDialog():
        def __init__(self, parent):
            self.parent = parent
        def OkCancelDialog(self, title, body):
            mb = QMessageBox(QMessageBox.Icon(), title, body, buttons=QMessageBox.Ok|QMessageBox.Cancel, parent=self.parent)
            r = mb.exec_()
            if r == QMessageBox.Ok:
                return PyQtUiPlugin.DIALOG_OK
            if r == QMessageBox.Cancel:
                return PyQtUiPlugin.DIALOG_CANCEL
        def OkDialog(self, title, body):
            mb = QMessageBox(QMessageBox.Icon(), title, body, buttons=QMessageBox.Ok, parent=self.parent)
            r = mb.exec_()
            return PyQtUiPlugin.DIALOG_OK

# This is the core class which starts QT engine, and loads the tabbed view:
class PyQtUiEngine(QWidget):
    def __init__(self, *args):
        QWidget.__init__(self, *args)
        layout = QVBoxLayout()                  # Main window rootlayout
        self.__tab = QTabWidget()               # Create single tab -widget
        self.__tab.setMinimumSize(640,480)      # Set minimum size
        layout.addWidget(self.__tab)            # Set tabview to rootlayout
        self.setLayout(layout)                  # Add the created layout to window
        self.keyStore = {}                      # Generate common key-value store for tab applications
        self.plugins = []                       # Store for each tab entity
    def loadPlugins(self, pluginDir):
        import importlib
        pList = os.listdir(pluginDir)
        pList.sort()
        for p in pList:
            if not p.endswith(".py"): continue
            plugin = p[:-3]
            try: module = __import__(p[:-3])
            except Exception as e:
                print ("Error, module '%s' failed to import! (%s)" % (plugin, repr(e)))
                continue
            tab = QWidget()
            c  = getattr(module, p[:-3])(parentEngine=self, parentWidget=tab)
            self.plugins.append(c)
            self.__tab.addTab(tab, c.getTitle())
    def onExit(self):
        for tab in self.plugins:
            try: tab.onExit()
            except: pass
    def addValueToStore(self, key, value):
        self.keyStore[key] = value
    def getValueFromStore(self, key):
        return self.keyStore.get(key, None)

if __name__ == "__main__":
    import getopt

    def usage():
        print ("USAGE: %s -p <plugindir> -H -V" % sys.argv[0])

    try:
        opts, args = getopt.getopt(sys.argv[1:], "p:HV", ["plugindir=", "maxhoriz", "maxvert"])
    except getopt.GetoptError as err:
        print ("Options error (%s)" % repr(err))
        usage()
        sys.exit(0)

    pluginDir = "plugins"
    maxHoriz = QSizePolicy.Ignored
    maxVert = QSizePolicy.Ignored
    for o, a in opts:
        if o in ("-p", "--plugindir"):
            pluginDir = str(a)
        if o in ("-H", "--maxhoriz"):
            maxHoriz = QSizePolicy.Maximum
            print("maxh")
        if o in ("-V", "--maxvert"):
            maxVert = QSizePolicy.Maximum

    app = QApplication(sys.argv)
    window = PyQtUiEngine()
    window.setSizePolicy(QSizePolicy(maxHoriz, maxVert))
    window.loadPlugins(pluginDir)
    window.show()
    rc = app.exec_()
    window.onExit()
    sys.exit(rc)
