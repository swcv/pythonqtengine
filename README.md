Python QT Engine
----------------

Python QT engine is a small tool to host QT based dialog applications. The core
idea is to create a tab view launcher, and then invoke specifed QT python applications
to the tabs. These applications are called plugins in this contest. Each plugin
can be a standalone application, and do not need to connect to other apps living
in other tabs. However, connection is also possible if needed, the undelying engine
implements methods for that too.

INSTALL
-------

The program is written using PyQt5 dependency. The development has been done on latest
Ubuntun, but it should work on Windows python too. It is recommended to use virtualenv
for the app. The virtual environment is created like this:

```bash
$ virtualenv env --system-site-packages -p python3
$ source env/bin/activate
$ pip3 install -r requirements.txt
$ PYTHONPATH=./plugins python3 PyQtUiEngine.py
```

The virtual env is deactivated using:

```bash
$ deactivate
```

Note, PyQt5 needs to be installed system wide, as pip3 does not know bullet proof way to
install it. On Ubunbu systems it works like this:

```bash
$ sudo apt-get install python3-pyqt5
```

RUNNING
-------

When the PyQtUiEngine is started it creates a context for running python QT (dialog)
applications. It creates the environment and then seeks the plugin folder for the
apps. It is expected that the classes written in the plugins are named in the same
way as the files they are stored in. Also, the classes myst inherit from 
PyQtUiPlugin class. Otherwise the engine is not able to launch them. The number of 
tabs is dynamic, i.e. at launch as many tabs are opened as plugins are found.

There are two examples: a hello world plugin which implements a push button and another
more complex example which creates multiple different QT widgets and layouts to the
dialog.