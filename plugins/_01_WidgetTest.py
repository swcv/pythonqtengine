#!/usr/bin/python3

from PyQtUiEngine import PyQtUiPlugin

from PyQt5.QtCore import *
from PyQt5.QtGui import *

from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QListWidget
from PyQt5.QtWidgets import QTableWidget
from PyQt5.QtWidgets import QTableWidgetItem
from PyQt5.QtWidgets import QTreeWidget
from PyQt5.QtWidgets import QTreeWidgetItem

class _01_WidgetTest(PyQtUiPlugin):

    PLUGINTITLE = "QT Widget/Layout test"

    def __init__(self, parentEngine, parentWidget):
        PyQtUiPlugin.__init__(self, self.PLUGINTITLE, parentWidget)
        self.parentEngine = parentEngine
        self.parentWidget = parentWidget

        #
        # Dialog layout:
        #

        _d = { 'type':'layout', 'widget':QHBoxLayout, 'content': [
                { 'type':'widget', 'widget':QWidget, 'content': [
                    { 'type':'layout', 'widget':QVBoxLayout, 'content': [
                        { 'type':'tree', 'widget':QTreeWidget, 'key':'grouptree', 'cbClick': self.__handleTreeviewClick,
                          'headerlabels':['ID','Name'] },
                        { 'type':'button', 'widget':QPushButton, 'name':'Button 1', 'cbClick': self.__handleButton1 },
                        { 'type':'button', 'widget':QPushButton, 'name':'Button 2', 'cbClick': self.__handleButton2 },
                        { 'type':'button', 'widget':QPushButton, 'name':'Button 3', 'cbClick': self.__handleButton3 },
                    ]}
                ]},
                { 'type':'widget', 'widget':QWidget, 'content': [
                    { 'type':'layout', 'widget':QVBoxLayout, 'content': [
                        { 'type':'widget', 'widget':QWidget, 'content': [
                            { 'type':'layout', 'widget':QHBoxLayout, 'content': [
                                { 'type':'label', 'widget':QLabel, 'name':'Test label:' },
                                { 'type':'label', 'widget':QLabel, 'name':'', 'key':'gidlabel' }
                            ]}
                        ]},
                        { 'type':'widget', 'widget':QWidget, 'content': [
                            { 'type':'layout', 'widget':QHBoxLayout, 'content': [
                                { 'type':'label', 'widget':QLabel, 'name':'Another test label:' },
                                { 'type':'label', 'widget':QLabel, 'name':'', 'key':'gnamelabel' }
                            ]}
                        ]},
                        { 'type':'table', 'widget':QTableWidget, 'key':'keyvalues', 'cbItemChanged': self.__handleTableWidget,
                          'headerlabels':['Key', 'Value']
                        },
                        { 'type':'widget', 'widget':QWidget, 'content': [
                            { 'type':'layout', 'widget':QHBoxLayout, 'content': [
                                { 'type':'button', 'widget':QPushButton, 'name':'New', 'cbClick': self.__handleNewKeyValue },
                                { 'type':'button', 'widget':QPushButton, 'name':'Edit',  'cbClick': self.__handleEditKeyValue },
                                { 'type':'button', 'widget':QPushButton, 'name':'Delete', 'cbClick': self.__handleDeleteKeyValue },
                            ]}
                        ]},
                        { 'type':'widget', 'widget':QWidget, 'content': [
                            { 'type':'layout', 'widget':QHBoxLayout, 'content': [
                                { 'type':'table', 'widget':QTableWidget, 'key':'usertree', 'cbClick': self.__handleTableWidget2 },
                                { 'type':'list', 'widget':QListWidget, 'key':'gwlist', 'cbClick': self.__handleGWList },
                            ]}
                        ]},
                        { 'type':'button', 'widget':QPushButton, 'key':'updatevalues', 'name':'Button 4', 'cbClick':self.__handleButton4
                        },
                    ]}
                ]}
            ]}

        #
        # Create the needed layout:
        #
        self.buildLayoutTree(parentWidget, _d)
        self.ignoreCellUpdate = False

    def __handleButton1(self):
        self.OkDialog("Button press", "You pressed button 1")

    def __handleButton2(self):
        self.OkDialog("Button press", "You pressed button 2")

    def __handleButton3(self):
        self.OkDialog("Button press", "You pressed button 3")

    def __handleButton4(self):
        self.OkDialog("Button press", "You pressed button 4")

    def __handleNewKeyValue(self):
        self.ignoreCellUpdate = True
        w = self.getValueFromLocalStore('keyvalues')
        c = w.rowCount()
        w.setRowCount(c+1)
        w.setItem(c, 0, QTableWidgetItem('New key'))
        self.ignoreCellUpdate = False
        w.setItem(c, 1, QTableWidgetItem('New value'))
        w.editItem(w.item(c, 0))

    def __handleEditKeyValue(self):
        w = self.getValueFromLocalStore('keyvalues')
        r = w.currentRow()
        if r == -1: return
        w.editItem(w.item(r, 0))

    def __handleDeleteKeyValue(self):
        w = self.getValueFromLocalStore('keyvalues')
        r = w.currentRow()
        if r == -1: return
        if PyQtUiPlugin.DIALOG_CANCEL == self.OkCancelDialog("Warning!", "Are you sure to delete %s/%s pair" % \
            (w.item(r, 0).text(), w.item(r, 1).text())):
            return
        w.removeRow(r)
        self.__handleKeyValueSelect()

    def __handleKeyValueSelect(self):
        if self.ignoreCellUpdate == True: return

        d = {}
        w = self.getValueFromLocalStore('keyvalues')
        for row in range(w.rowCount()):

            k = str(w.item(row, 0).text())
            if k in d.keys():
                self.OkDialog("Warning", "Warning, there cannot be duplicate keys in key/value store!")
                return
            if k == '':
                self.OkDialog("Warning", "Warning, there cannot be empty keys in key/value store!")
                return
            v = str(w.item(row, 1).text())
            try: a = int(v)
            except:
                try: a = float(v)
                except: a = v
            d[k] = a


    def __handleTableWidget(self):
        return

    def __handleTableWidget2(self):
        return

    def __handleGWList(self):
        self.OkDialog("Listclick", "List widget clicked!")

    def __handleUpdateValues(self):
        return

    def __handleRefreshGroups(self):
        return

    def __handleTreeviewClick(self):
        self.OkDialog("Treeview click", "You clicked the treeview!")

    def __handleDeleteGroup(self):
        return

    def __handleNewGroup(self):
        return

    def __cleraGroupWidgetArea(self):
        return
