#!/usr/bin/python3

from PyQtUiEngine import PyQtUiPlugin

from PyQt5.QtCore import *
from PyQt5.QtGui import *

from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QLabel

class _00_HelloWorld(PyQtUiPlugin):

    PLUGINTITLE = "Hello World"

    def __init__(self, parentEngine, parentWidget):
        PyQtUiPlugin.__init__(self, self.PLUGINTITLE, parentWidget)
        self.parentEngine = parentEngine
        self.parentWidget = parentWidget

        #
        # Dialog layout:
        #

        _d = { 'type':'layout', 'widget':QVBoxLayout, 'content': [
                { 'type':'label',  'widget':QLabel, 'name':'Hint: Press the button below' },
                { 'type':'button', 'widget':QPushButton, 'name':'Secret button', 'cbClick': self.__handleHelloWorld }
                ]
             }

        #
        # Create the needed layout:
        #
        self.buildLayoutTree(parentWidget, _d)


    # Callback for hello world button:
    #
    def __handleHelloWorld(self):
        self.OkDialog("Hello world!", "This really is hello world!")
